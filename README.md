<h1 align="center">MiFare Card Cracker</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

### 项目介绍

使用`PN532`对`MiFare (M1) 卡`进行暴力破解

### 适用人群

本程序适人群为：

1. 手中正好有一块`PN532 开发板`
2. 碰到一张`国产无漏洞卡`
3. 不想花大价钱购入更专业的`设备和软件`

### 运行（测试）环境

* Windows 10
* Python 3.8
* PySerial 3.5

> 不支持 Python 2 运行

> Linux 环境未测试

### 使用方法

使用如下代码运行程序

```shell
python cracker.py
```

运行后会列出除`COM1`之外的所有串口设备，用数字和回车进行选择确认

> 提示：直接按回车键选择`第一个`设备

接下来输入起始秘钥（范围`0 ~ 0xffffffffffff`之间）

> 重要：秘钥测试顺序为`倒序`，例如起始秘钥为`9`，则下一个测试的秘钥为`8`

> 提示：直接按回车键输入`默认最大值`

之后程序开始检测卡片，放置卡片后会使用默认秘钥对所有扇区进行检测，如果：

* 默认秘钥可以解开所有扇区代表这是一张未加密的卡片，程序退出
* 默认秘钥无法解开的话会提示用户选择一个已加密的扇区进行破解

    > 提示：直接按回车键选择`第一个`扇区

全部完成后程序退出

### 完整的输出内容

```docs
λ python cracker.py
# Serial ports:
    (1) USB-SERIAL CH340 (COM3)
# Choose a port:
# Found PN532 with firmware version: 1.6
# Input start key: 98af5fd83c96
# Waiting for MiFare card...

# Found card:
    ATQA (SENS_RES): 00 04
    SAK (SEL_RES): 08
    UID: e1 e5 67 a4
# Try to decrypt sectors using default key
    sector 0: ok
    sector 1: ok
    sector 2: ok
    sector 3: ok
    sector 4: ok
    sector 5: ok
    sector 6: ok
    sector 7: ok
    sector 8: ok
    sector 9: ok
    sector 10: ok
    sector 11: ok
    sector 12: ok
    sector 13: bad
    sector 14: bad
    sector 15: bad

# Choose a sector [13, 14, 15]:
# Cracking encrypted sector 13...
### Found key for sector 13: 98af5fd83c86
# Sector 13 cracked, wasted 7 seconds in this time

# Choose a sector [14, 15]:
# Cracking encrypted sector 14...
### Found key for sector 14: 98af5fd83c86
# Sector 14 cracked by using known keys

# Choose a sector [15]:
# Cracking encrypted sector 15...
### Found key for sector 15: 98af5fd83c86
# Sector 15 cracked by using known keys

# All sectors of card [e1e567a4] were crack completed
# Results:
    Sector 13: 98af5fd83c86
    Sector 14: 98af5fd83c86
    Sector 15: 98af5fd83c86

```

### 破解中的操作提示

#### 查看进度

为了提高一定的效率，在正式破解期间是没有任何提示输出的，如果你想知道进度怎么办？

这时只需要把卡片从设备上拿开，破解就会暂停并显示当前正在尝试破解的秘钥

> 存在的问题：当前秘钥和之前一个秘钥有可能因为卡片移开的时机问题，导致验证结果不正确，也就是说每次移开卡片都有可能导致 1~2 个秘钥破解失败

#### 中断破解

暴力破解肯定是一个漫长的过程，如果需要临时中断破解进程，只需要按下`ctrl + c`中断程序运行即可，你可以选择记录屏幕上输出的当前秘钥，也可以事后打开`log_file.txt`文件查找记录

然后重新运行程序，在

```docs
# Input start key:
```

后边输入中断之前保存的秘钥继续破解过程

> 存在的问题：同上，但是你可以手动修正这个问题了

### 已知问题

当然就是效率低下的问题，由于连续对扇区进行解密测试会出现解密结果反馈不正常的问题，所以不得不调用了一些有用没用的函数以确保结果正确，最终导致了效率低下，目前肉眼观察效率为`1 key/s`，目前无解[摊手]

### PN532 驱动说明

[原 PN532 驱动](https://github.com/mfdogalindo/PN532-HSU) 为`Python 2`版本，为了方便使用已经改为`Python 3`版本，且不兼容`Python 2`

### 合作及交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
    * [走线物联](https://jq.qq.com/?_wv=1027&k=xtPoHgwL)：163271910
    * [扇贝物联](https://jq.qq.com/?_wv=1027&k=yp4FrpWh)：31324057

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>
