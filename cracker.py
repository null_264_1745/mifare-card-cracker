# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright © 2021 Walkline Wang (https://walkline.wang)
# https://gitee.com/walkline/mifare-card-cracker

"""
使用 PN532 对 MiFare (M1) 卡进行暴力破解

使用如下代码运行程序

	python cracker.py

运行后会列出除 COM1 之外的所有串口设备，用数字和回车进行选择确认

> 小提示：直接按回车键选择`第一个`设备

接下来输入起始秘钥（范围 0 ~ 0xffffffffffff 之间）

> 重要：秘钥测试顺序为 倒序，例如起始秘钥为 9，则下一个测试的秘钥为 8
> 小提示：直接按回车键输入 默认最大值

之后程序开始检测卡片，放置卡片后会使用默认秘钥对所有扇区进行检测，如果：

* 默认秘钥可以解开所有扇区代表这是一张未加密的卡片，程序退出
* 默认秘钥无法解开的话会提示用户选择一个已加密的扇区进行破解

    > 小提示：直接按回车键选择 第一个 扇区

全部完成后程序退出
"""

import sys
import time
import PN532
from datetime import datetime
import serial.tools.list_ports


MAX_KEY_VALUE = 0xffffffffffff

def write_log(msg, log_file='log_file.txt'):
	"""
	向指定文件写入 log 记录
	"""
	now = datetime.strftime(datetime.now(),'%Y-%m-%d %H:%M:%S')

	with open(log_file, 'a+') as log:
		log.write('[{}] {}\n'.format(now, msg))

def list_serial():
	"""
	列出系统中所有串口（COM1 除外）
	"""
	port_list = list(serial.tools.list_ports.comports())

	for port in port_list:
		if port[0] == 'COM1':
			port_list.remove(port)

	return port_list if len(port_list) > 0 else None

def get_pn532_instance(port='COM3'):
	"""
	获取 PN532 设备串口实例
	"""
	try:
		pn532 = PN532.PN532(port, 115200)
		pn532.begin()
		pn532.SAM_configuration()

		ic, ver, rev, support = pn532.get_firmware_version()
		print('# Found PN532 with firmware version: {}.{}'.format(ver, rev))
	except Exception:
		print('# Cannot found PN532 or open serial port failed')
		sys.exit()

	return pn532

def get_serial_port():
	"""
	获取用户选择的串口号（按回车键默认选择第一项）
	"""
	print('# Serial ports:')

	ports = list_serial()
	index = 0
	port_index = 0xff

	if ports:
		for port in ports:
			index += 1
			print('    ({}) {}'.format(index, port[1]))

		while not 0 < port_index <= index:
			port_index = input('# Choose a port: ')

			if port_index == '' and index >= 1:
				port_index = 1
				break

			port_index = 0xff if not port_index.isdigit() else int(port_index)
	else:
		print('# No serial port found')
		sys.exit()

	return ports[port_index - 1][0]

def get_start_key():
	"""
	获取用户输入的起始测试秘钥（按回车键默认输入秘钥最大值）
	"""
	start_key = None

	while start_key is None:
		start_key = input('# Input start key: ')

		if start_key == '':
			start_key = MAX_KEY_VALUE
			break

		try:
			start_key = int(start_key)
			if start_key < 0 or start_key > MAX_KEY_VALUE: start_key = None
		except ValueError:
			try:
				start_key = int(start_key, 16)
				if start_key < 0 or start_key > MAX_KEY_VALUE: start_key = None
			except ValueError:
				start_key = None

	return start_key

def get_bad_sector_list(pn532:PN532.PN532):
	"""
	获取卡片上已加密的扇区列表
	"""
	print('# Waiting for MiFare card...')

	uid = None
	while True:
		sens_res, sel_res, uid = pn532.read_passive_target()
		if uid not in ('no_card', None):
			print('\n# Found card:\n    ATQA (SENS_RES): {}\n    SAK (SEL_RES): {}\n    UID: {}'.format(
				sens_res,
				sel_res,
				' '.join(str(hex(bit).strip('0x')).zfill(2) for bit in uid)
			))
			break

	print('\n# Try to decrypt sectors using default key')

	bad_sector_list = []
	cracked_count = 0

	for count in range(0, 16):
		if pn532.listen_for_passive_target() in ('no_card', None):
			print('Card removed, waiting for card to continue...')

			while True:
				_, _, uid = pn532.read_passive_target()
				if uid not in ('no_card', None):
					break

		if pn532.mifare_classic_authenticate_block(
			uid, count * 4,
			PN532.MIFARE_CMD_AUTH_A & PN532.MIFARE_CMD_AUTH_B,
			[0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
		):
			print('    sector {}: ok'.format(count))
			cracked_count +=1
		else:
			print('    sector {}: bad'.format(count))
			bad_sector_list.append(count)

	if cracked_count == 16:
		print('# Unencrypted card, cracking terminated')
		sys.exit()

	return bad_sector_list, uid

def get_bad_sector(bad_sector_list=[]):
	"""
	获取用户选择的待破解扇区号（按回车键默认选择第一项）
	"""
	selected_sector = -1

	while selected_sector not in bad_sector_list:
		selected_sector = input('\n# Choose a sector {}: '.format(bad_sector_list))

		if selected_sector == '':
			selected_sector = bad_sector_list[0]
			break

		if not selected_sector.isdigit():
			selected_sector = -1
		else:
			if int(selected_sector) not in bad_sector_list:
				selected_sector = -1
			else:
				selected_sector = int(selected_sector)

	return selected_sector

def crack_sector(pn532:PN532.PN532, uid, selected_sector, known_keys=None, start_key=MAX_KEY_VALUE):
	"""
	使用已获取的参数破解指定扇区，如果存在已破解秘钥则使用该秘钥进行尝试破解
	"""
	def keys_generator(start_key):
		while start_key >= 0:
			yield start_key
			start_key -= 1

	print('# Cracking encrypted sector {}...'.format(selected_sector))

	key_counts = keys_generator(start_key) if known_keys is None else known_keys

	for key_count in key_counts:
		try:
			if pn532.listen_for_passive_target() in ('no_card', None):
				print('# Current key: {}'.format(hex(key_count).strip('0xL')))
				print('# Card removed, waiting for card to continue...')

				while True:
					_, _, uid = pn532.read_passive_target()
					if uid not in ('no_card', None):
						print('# Cracking continue...')
						break

			key_bytes = []
			key_string = hex(key_count).strip('0xL').zfill(12)

			for count  in range(0, 12, 2):
				key_bytes.append(int(key_string[count: count+2], base=16))

			if pn532.mifare_classic_authenticate_block(
				uid, selected_sector * 4,
				PN532.MIFARE_CMD_AUTH_A & PN532.MIFARE_CMD_AUTH_B,
				key_bytes
			):
				msg = '### Found key for sector {}: {}'.format(selected_sector, hex(key_count).strip('0xL'))
				print(msg)
				write_log(msg)

				return True, hex(key_count).strip('0xL')
		except KeyboardInterrupt as kbi:
			msg = '# Cracking interrupted, current key: {}'.format(hex(key_count).strip('0xL'))
			print(msg)
			write_log(msg)
			sys.exit()
		except RuntimeError as rte:
			print('# PN532 error: {}'.format(str(rte)))
		except Exception as e:
			print('# Exception: {}'.format(str(e)))

			msg = '# Cracking interrupted, current key: {}'.format(hex(key_count).strip('0xL'))
			print(msg)
			write_log(msg)

	return False, None

def start():
	serial_port = get_serial_port()
	pn532 = get_pn532_instance(serial_port)
	start_key = get_start_key()
	bad_sector_list, uid = get_bad_sector_list(pn532)
	crack_results = []

	while len(bad_sector_list) > 0:
		selected_sector = get_bad_sector(bad_sector_list)
		known_keys=[int(result[1], 16) for result in crack_results]
		start_time = datetime.now()

		if len(known_keys) > 0:
			result, correct_key = crack_sector(
				pn532,
				uid=uid,
				selected_sector=selected_sector,
				known_keys= known_keys,
				start_key=start_key
			)

			if result:
				crack_results.append((selected_sector, correct_key))
				bad_sector_list.remove(selected_sector)

				msg = '# Sector {} cracked by using known keys'.format(selected_sector)
				print(msg)
				write_log(msg)
				continue

		result, correct_key = crack_sector(
			pn532,
			uid=uid,
			selected_sector=selected_sector,
			start_key=start_key
		)

		if result:
			crack_results.append((selected_sector, correct_key))
			bad_sector_list.remove(selected_sector)

			msg = '# Sector {} cracked, wasted {} seconds in this time'.format(
				selected_sector,
				(datetime.now() - start_time).seconds)
			print(msg)
			write_log(msg)

	print('\n# All sectors of card [{}] were crack completed'.format(''.join(str(hex(bit).strip('0x')).zfill(2) for bit in uid)))
	print('# Results:')

	for result in crack_results:
		print('    Sector {}: {}'.format(result[0], result[1]))


if __name__ == '__main__':
	import sys

	if sys.version_info.major == 3:
		start()
	else:
		print('python2 not support')
